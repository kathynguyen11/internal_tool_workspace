@extends('base')

@section('main')

<div>
    <a style="margin: 19px;" href="{{ route('feeds.create')}}" class="btn btn-primary">Add new feed</a>
</div>
<div class="row">
    <div class="col-sm-12">
        <h1 class="display-3">Feeds</h1>
        <table class="table table-striped">
            <thead>
            <tr>
                <td>Title</td>
                <td>Url</td>
                <td colspan=2>Actions</td>
            </tr>
            </thead>
            <tbody>
            @foreach($feeds as $feed)
                <tr>
                    <td>{{$feed->Title}}</td>
                    <td>{{$feed->URL}}</td>
                    <td>
                        <a href="{{ route('feeds.show',$feed->Id)}}" class="btn btn-primary">Get items</a>
                    </td>
                    <td>
                        <form action="{{ route('feeds.destroy' , $feed->Id ) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button>Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div>
        </div>
    </div>
</div>
@endsection