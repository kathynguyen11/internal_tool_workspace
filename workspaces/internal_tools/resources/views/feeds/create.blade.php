@extends('base')

@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Add feed</h1>
        <div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br/>
            @endif
            <form action="{{ route('feeds.store') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="first_name">Title:</label>
                    <input type="text" class="form-control" name="title"/>
                </div>

                <div class="form-group">
                    <label for="last_name">URL:</label>
                    <input type="text" class="form-control" name="url"/>
                </div>
                <button type="submit" class="btn btn-primary-outline">Add feed</button>
        </div>
    </div>
</div>
@endsection