@extends('base')

@section('main')
<div class="row">
    <div class="col-sm-12">
        <h1 class="display-3">Feeds</h1>
        <table class="table">
            <thead>
            <tr>
                <td>Title</td>
                <td>Publish Date</td>
                <td>Url</td>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <td>{{$item[0]}}</td>
                    <td>{{$item[1]}}</td>
                    <td>
                        <a href="{{$item[2]}}" class="btn btn-primary" target="_blank">{{$item[2]}}</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div>
        </div>
    </div>
</div>
@endsection