## Tutorial

####For the CLI - mode

- For show list of feed:  php artisan feed:manage view
- Display current item of feed 
    - From id : php artisan feed:manage reader --id=
    - From url: php artisan feed:manage reader --url=
- Remove feed: php artisan feed:manage delete --id=
- Add a new feed: php artisan feed:manage add --title= --url= 

####For the GUI

- Install docker for run project
- Go to *localhost/feeds* to use

####Note
- I'm use my current personal workspace for implement this test. So please ignore the name of project. Thank you very much.