<?php

namespace App\Http\Controllers;

use App\Feeds;
use App\Service\FeedService;
use Illuminate\Http\Request;
use Illuminate\View\View;

class FeedController extends Controller
{

    protected static $service;

    public function __construct(FeedService $feedService)
    {
        self::$service = $feedService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feeds = self::$service->viewFeeds();

        return view('feeds.index', compact('feeds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('feeds.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'url' => 'required'
        ]);
        $title = $request->get('title');
        $url = $request->get('url');
        if (isset($title) && isset($url)) {
            self::$service->addFeed($title, $url);
        }

        return redirect('/feeds')->with('success', 'Feed saved!');
    }

    /**
     * @param $url
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function show($id)
    {
        $feed = Feeds::find($id);
        $url = $feed->url;

        $items = self::$service->parseRSS($url);

        return view('feeds.items', compact('items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feed = Feeds::find($id);
        $feed->delete();

        return redirect('/feeds')->with('success', 'Feed deleted!');

    }
}
