<?php

namespace App\Service;

use App\Feeds;

class FeedService
{

    public function addFeed($title,$url)
    {
        $feed = new Feeds([
            'title' => $title,
            'url' => $url
        ]);
        $feed->save();
    }

    public function viewFeeds()
    {
        return Feeds::all(['Id','Title','URL']);
    }

    public function parseRSS($url)
    {
        $feed = implode(file($url));
        $xml = simplexml_load_string($feed);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);

        $rssList = $array['channel']['item'];
        $rssListFormatted = [];
        foreach ($rssList as $item) {
            if (is_array($item) && isset($item['title']) && isset($item['pubDate']) && isset($item['link'])) {
                array_push($rssListFormatted, [$item['title'], $item['pubDate'], $item['link']]);
            }
        }


        return $rssListFormatted;
    }

    public function removeFeed($id)
    {
        $feed = Feeds::find($id);

        if(!isset($feed))
        {
            echo 'Feed not found';
        }else{
            echo 'Deleted successfully';
            $feed->delete();
        }
    }
}
