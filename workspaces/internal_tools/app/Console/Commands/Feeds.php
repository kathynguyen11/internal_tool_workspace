<?php

namespace App\Console\Commands;

use App\Service\FeedService;
use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\Table;

class Feeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:manage 
                            {action : Add/view/delete/reader}
                            {--title= : The title of feed.}
                            {--id= : The id of feed.}
                            {--url= : The url of feed}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Commands to manage feeds';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $feedService = new FeedService();
        $action = $this->argument('action');
        switch ($action) {
            case 'add':
                $title = $this->option('title');
                $url = $this->option('url');

                if (isset($title) && isset($url)) {
                    $feedService->addFeed($title, $url);
                }
                echo 'New feed added';

                break;
            case 'delete':
                $id = $this->option('id');
                $feedService->removeFeed($id);

                break;
            case 'view':
                $feeds = $feedService->viewFeeds();
                $headers = ['Id', 'Title', 'URL'];

                $this->table($headers, $feeds);

                break;
            case 'reader':
                $id = $this->option('id');
                $url = $this->option('url');
                if (!isset($url)) {
                    if (isset($id)) {
                        $feed = \App\Feeds::find($id);
                        if (!$feed) {
                            echo "Feed not found";
                            break;
                        }
                        $url = $feed->url;
                    }
                }

                $array = $feedService->parseRSS($url);

                $table = new Table($this->output);
                $table
                    ->setHeaders(['Title', 'Publish Date', 'URL'])
                    ->setRows($array);
                $table->render();
                break;
        }
    }
}
